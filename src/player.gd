extends Node2D

var speed = 100

func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	position.x += speed * delta *  x
	
	var y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	position.y += speed * delta *  y
	
	
