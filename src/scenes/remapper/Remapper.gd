extends Control

onready var msg = $Label
var a = 0
var actions = ["ui_right","ui_left","ui_up","ui_down"]
var current = ""
var mapped = ""

func _process(delta):
	msg.text = "Press Key/Button for twice " + actions[a]
	msg.text += "\nInput Pressed: " + current
	msg.text += "\nMapped: " + mapped
	 
func _input(event):
	if !event.is_pressed():
		return

	if event.as_text() != current:
		current = event.as_text()
		return
		
	if event is InputEventKey || event is InputEventJoypadButton:
		
		#print(event.as_text())
		#uncomment the following if you want to erase old control action
		#InputMap.action_erase_events(actions[a])
		InputMap.action_add_event(actions[a], event)
		mapped += "\n  " + actions[a] + " : " + event.as_text()
		a+=1
		if a >= actions.size():
			get_tree().change_scene("res://main.tscn")
		
